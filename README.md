# TP2 Interface web

## Installation des dépendances
---
	npm install
## Lancement du Projet
---
Pour lancer le projet seulement utiliser la commande.

	npm start

Une fois lancer, les différents liens devrait s'afficher dans la console.

Ceux-ci sont corresponde à [http://localhost:8080](http://localhost:8080) en local et `http://[ip de votre machine]:8080` pour tester sur votre appareil mobile.

<br>

## Connaitre son ip local windows
---
Vous pouvez trouver l'ip de votre machine depuis un terminal de commande.
1. Windows
	1. Ouvrir un terminal `windows + x, i` sur windows 10
	2. Lancer la commande `ipconfig`
	3. Regarder la ligne nommé `Adresse IPV4`
2. Linux
	1. Ouvrir un terminal `ctrl + alt + t` sur ubuntu
	2. Lancer la commande `ifconfig`
	3. Regarder la valeur suivant le tag `inet addr:`

<br>

## Information global du projet
---
La page principale du projet correspond a l'interface 1 du devoir.

L'interface 2 est accessible via **tous les liens disponible sur l'interface 1** ainsi que directement depuis la barre de navigation avec l'extension `/interface2`
